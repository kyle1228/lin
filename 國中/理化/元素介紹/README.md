# 元素介紹

## 元素週期表

![Simple_Periodic_Table_Chart-en](Simple_Periodic_Table_Chart-en.svg)

由 <a href="//commons.wikimedia.org/wiki/User:Offnfopt" title="User:Offnfopt">Offnfopt</a> - <span class="int-own-work" lang="zh-tw">自己的作品</span>, 公有領域, <a href="https://commons.wikimedia.org/w/index.php?curid=62296883">連結</a>

## 1-H (氫)

$${^{1}_{1}H}$$ ：最輕的原子，由一個原子核有1個質子，並且沒有中子。

同位素：氘，$${^2_1H}$$ or $${D}$$、氚，$${^3_1H}$$ or $${T}$$

離子：為 $${H+}$$，因此常見狀態為雙原子分子，氫氣 $${H2}$$

###哈佛大學：金屬氫

https://youtu.be/1qitm5fteL0

<iframe width="560" height="315" src="https://www.youtube.com/embed/1qitm5fteL0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 金屬氫

https://youtu.be/FYOuQ84-6Z0

<iframe width="560" height="315" src="https://www.youtube.com/embed/FYOuQ84-6Z0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



## 2-He(氦)

## 3-Li(鋰)

## 4-Be(鈹)

## 5-B(硼)

## 6-C(碳)

## 7-N(氮)

## 8-O(氧)

## 9-F(氟)
https://youtu.be/22VJ43tMlJ8

<iframe width="560" height="315" src="https://www.youtube.com/embed/22VJ43tMlJ8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



**Contributer**:**王泓譁**

## 10-Ne(氖)

## 11-Na(鈉)

## 12-Mg(鎂)

## 13-Al(鋁)
https://youtu.be/X3A-KvnHN0U

<iframe width="560" height="315" src="https://www.youtube.com/embed/X3A-KvnHN0U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Contributer**:**王泓譁**

## 14-Si(矽)

## 15-P(磷)

## 16-S(硫)

## 17-Cl(氯)
https://youtu.be/BXCfBl4rmh0

<iframe width="560" height="315" src="https://www.youtube.com/embed/BXCfBl4rmh0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



**Contributer**:**曾韋騰**

## 18-Ar(氬)

## 19-K(鉀)

## 20-Ca(鈣)

## 35-Br(溴)
https://youtu.be/1CtnJtYcm_M

<iframe width="560" height="315" src="https://www.youtube.com/embed/1CtnJtYcm_M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



**Contributer**:**曾韋騰**

## 53-I(碘)
https://youtu.be/_e194ZAxwko

<iframe width="560" height="315" src="https://www.youtube.com/embed/_e194ZAxwko" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Contributer**:**曾韋騰**

## 74-W(鎢)
https://youtu.be/6XxcVg2Dfck

<iframe width="560" height="315" src="https://www.youtube.com/embed/6XxcVg2Dfck" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





**Contributer**:**曾韋騰**








