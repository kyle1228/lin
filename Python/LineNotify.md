# 手把手教你 Line Notify

## Outline

這篇教學會告訴你如何利用 Python 做出簡易的Line Notify。你需要的東西有：

- Python （這裡用Colab 做示範）
- Line 帳號

## 什麼是Line Notify?

這邊很簡單的跟大家解釋Line Notify的作用。簡單來說，我們可以透過網路，讓Line送訊息到妳指定的群組中。訊息包括文字、圖片都可以。常見的應用方式有伺服器狀態的更新、每日天氣通知、Gmail相關、IG相關等等。你可以用[IFTTT](https://ifttt.com)來實現很多功能，而不需要寫任何程式。

Line Notify 是單向的，什麼意思呢？就是她只能傳訊息，而不能接收訊息。你可能看過「卡米狗」，你可以跟他對話，他會有相對應的回答。或是一些官方帳號，可以做一些互動設計。Line Notify 只能單純傳訊息。若你想做互動型的、很多功能的，那是「Line Bot」。

## 取得Line Notify 權杖

### 什麼是權杖

妳可以把它想成一個識別碼、郵遞區號+地址的概念。當你寫了一封信（要傳的訊息或圖片）需要告訴Line 到底要傳到哪個群組去，這時候就需要權杖。

### 發行權杖

首先進入[Line Notify網站](https://notify-bot.line.me/zh_TW/)並登入。按右上方名字的地方，進入「個人頁面」。![截圖 2021-02-15 下午11.01.50](截圖 2021-02-15 下午11.01.50.png)

進入後，會有「已連動的服務」（但如果你是第一次用，應該不會有。）以及「發行存取權杖」，我們點擊「發行權杖」。![截圖 2021-02-15 下午11.04.06](截圖 2021-02-15 下午11.04.06.png)

選擇想要傳送訊息的群組。這邊為了教學所以我創立了一個群組。名稱可以自訂。填完就按「發行」。

![截圖 2021-02-15 下午11.08.08](截圖 2021-02-15 下午11.08.08.png)

發行完之後會出現一組英文+數字，**重要！！這個只會出現一次**，不見的話就要重新發行。我們先把他複製起來。（保險起見，你可以先貼到記事本裡面。）

![截圖 2021-02-15 下午11.12.33](截圖 2021-02-15 下午11.12.33.png)

到這邊為止，你已經取得權杖了，再來我們要進入到程式碼的部分。



修但幾咧！很重要一點！！

你必須把Line Notify 邀請進入你剛剛指定的那個群組中！

## 用Python發送Line Notify訊息

打開[這個範例](https://colab.research.google.com/drive/1Lug8OH_XPaT-NwjYubAB5kkHJ7CAqvop?usp=sharing)並複製它到一個新的Colab記事本中。

或直接複製下方：

```python
import requests

def LineMessage(token, msg):
	headers = {
		"Authorization": "Bearer "+token,
		"Content-Type":"application/x-www-form-urlencoded"
	}
	payload = {
      'message' : msg,
					}

	r = requests.post('https://notify-api.line.me/api/notify', headers = headers, params = payload)
	return r.status_code



message = '這裡改成你想傳送的訊息'
token = '這邊改成你自己發行的權杖'

print(LineMessage(token, message))
```



更改`message`成你想傳送的訊息、`token`改成你自己的權杖，其他都不要動。

![截圖 2021-02-15 下午11.22.41](截圖 2021-02-15 下午11.22.41.png)

執行結果![截圖 2021-02-15 下午11.20.43](截圖 2021-02-15 下午11.20.43.png)