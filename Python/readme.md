# Python 簡介

## Python 可以做什麼?

###1. 將無聊的事情「自動化」

電腦程式的一個最簡單的應用方式，就是將繁瑣但是有規律的事情給自動化處理。

一個簡單的例子是，當我們有一堆照片要重新命名的時候，如果一張一張處理，非常耗時間，但如果我們將處理方式的邏輯寫成程式，那麼只要一眨眼的時間就可以完成所有工作！

**以學生照片由學號轉為班級+座號為例**

### 2. 建立自己的「Line Notify」

有看過利用Line定期推播的星座運勢嗎？自己做一個吧！

[**以推播梗圖為例**](https://colab.research.google.com/drive/1-DmRmojvTyVg4clF6JAHGZihWtKQMEqj?usp=sharing)

[點此加入梗圖群組](http://line.me/ti/g/ZS0_owQMtE)

註：與「Line Bot」不同

### 3. 數據分析

你有聽過「近十年學測高頻率單字」嗎？

如果一字一句地看，要花多久時間呢？

我們可以利用Python來幫助我們快速的分析你想知道的結果！

[**以文章之單字統計為例**](https://colab.research.google.com/drive/1uM37iZulVX4Ezyln9q7UZWPyZxibOwLG?usp=sharing)

### 4. 資訊安全--加解密

你一定有解過「線型函數」，但是你知道這可以被用做電腦「加密資料」的原理嗎？

「將一個秘密分割成若干的資訊，且任意兩個人(以上)才可以解出此秘密。」

**以(2,k)加密為例**

你知道古代如何傳遞秘密而不讓人輕易發覺嗎？讓我們看看「凱薩加密法」

[**以凱薩密碼法為例**](https://colab.research.google.com/drive/1l4_7UGFhWcYxGM0Xi6YiYM1Jvx7WANGk?usp=sharing)

我們日常中，總有一些字特別常說到，你知道最常用到的英文字母是「e」嗎？

我們可以透過這個觀察，來解出一些加密過的文字。

**以頻率分析法為例**

### 其他

[**猜數字遊戲**](https://colab.research.google.com/drive/1fSfCUqxbc_nSKZZjPcyi8HiYqbRVKfNA?usp=sharing)

[**考單字**](https://colab.research.google.com/drive/1a381wG7KMupiT7cx9okup8TB3bnzjrS2?usp=sharing)

**斐波那契數列**

**[約瑟夫問題](https://colab.research.google.com/drive/1DzWOVeb0HmFsBGFUH0vFRRJWGtKCSyyN?usp=sharing)**



