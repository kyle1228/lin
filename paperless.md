# paperless 無紙化

> Author : 林冠曄
>
> Last update : 2019/11/29

## About

自從2019三月買了ipad 就開始研究無紙化的教學、學習等等。這邊記錄一些心得跟軟體。

## Things I am using

###Hardware

1. 2018 iPad 9.7" 128GB
2. Apple Pencil Gen 1
3. MacBook Pro 2018

### Software

1. [GoodNotes5](https://www.goodnotes.com) - $300 - on iPad
2. [Notability](https://www.gingerlabs.com) - $270 - on iPad
3. [PDFViewer](https://pspdfkit.com/guides/ios/current/faq/pdf-viewer/) - free - on iPad (subscribe pro version available)
4. [MarginNote3](https://www.marginnote.com) - $430 - on iPad
5. [Agenda](https://agenda.com) - free - on ipad, iPhone, MacBook
6. [GeoGebra](https://www.geogebra.org) - free - on iPad, iPhone, MacBook
7. Pages - iOS build-ins