# 高一高二第三次段考複習150題檔案

> 2021.1.3

[高二3-1題目](高二3-1題目.pdf)

[高二3-1答案](高二3-1答案.pdf)

[高二3-2題目](高二3-2題目.pdf)

[高二3-2答案](高二3-2答案.pdf)

[高二3-3題目](高二3-3題目.pdf)

[高二3-3答案](高二3-3答案.pdf)

[高一4-1題目](高一4-1題目.pdf)

[高一4-1答案](高一4-1答案.pdf)

[高一4-2題目](高一4-2題目.pdf)

[高一4-2答案](高一4-2答案.pdf)

[高一4-3題目](高一4-3題目.pdf)

[高一4-3答案](高一4-3答案.pdf)





{% pdf src="高二3-1題目.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高二3-1答案.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高二3-2題目.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高二3-2答案.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高二3-3題目.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高二3-3答案.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高一4-1題目.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高一4-1答案.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高一4-2題目.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高一4-2答案.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高一4-3題目.pdf", width="100%", height="850" %}{% endpdf %}

{% pdf src="高一4-3答案.pdf", width="100%", height="850" %}{% endpdf %}